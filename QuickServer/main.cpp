#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "./socket/csourceserver.h"
#include "./socket/cuserserver.h"

#include <QDebug>
#include <QQmlContext>
#include "afxpublic.h"

#include <QJsonParseError>
#include <QVariant>
#include "./xml/xmlstreamreader.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    CSourceServer   sourServer;
    CuserServer     userServer;

    if(!sourServer.listen(QHostAddress::Any,60000))// QHostAddress::Any
    {
        qDebug() << "60000 be defeated";
    }
    else
        qDebug() << "60000 succeed";

    if(!userServer.listen(QHostAddress::Any,60001))// QHostAddress::Any
    {
        qDebug() << "60001 be defeated";
    }
    else
        qDebug() << "60001 succeed";
    XmlStreamReader xml;
    xml.readFile("text.xml");
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("myRecv",          CMainRecv::getInstance());
    engine.rootContext()->setContextProperty("scourceSocket",   CSourceMap::getInstance());
    engine.rootContext()->setContextProperty("userSocket",      CUserMap::getInstance());
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

