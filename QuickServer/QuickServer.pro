TEMPLATE = app

QT += qml quick
QT += sql

CONFIG += c++11

SOURCES += main.cpp \
    framework/subject.cpp \
    framework/observer.cpp \
    socket/csourceclient.cpp \
    socket/csourceserver.cpp \
    socket/cuserserver.cpp \
    socket/cuserclient.cpp \
    map/csocketmap.cpp \
    map/cusermap.cpp \
    map/csourcemap.cpp \
    cmainrecv.cpp \
    db/dbopt.cpp \
    ini/config.cpp \
    xml/xmlstreamreader.cpp \
    xml/xmloperator.cpp \
    xml/xmlopt.cpp \
    modbus/canalysis.cpp \
    modbus/cdualsys.cpp \
    modbus/csinglesys.cpp \
    afxpublic.cpp \
    json/jsonstreamreader.cpp \
    json/jsonoperator.cpp \
    json/jsonopt.cpp \
    analysisstreamreader.cpp \
    modbus/cdualsysext.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    framework/subject.h \
    framework/observer.h \
    socket/csourceclient.h \
    socket/csourceserver.h \
    socket/cuserserver.h \
    socket/cuserclient.h \
    map/csocketmap.h \
    map/cusermap.h \
    map/csourcemap.h \
    cmainrecv.h \
    afxpublic.h \
    db/dbopt.h \
    ini/config.h \
    xml/xmlstreamreader.h \
    xml/xmloperator.h \
    xml/xmlopt.h \
    modbus/canalysis.h \
    modbus/cdualsys.h \
    modbus/csinglesys.h \
    modbus/crc16.h \
    modbus/myhelper.h \
    json/jsonstreamreader.h \
    json/jsonoperator.h \
    json/jsonopt.h \
    analysisstreamreader.h \
    modbus/cdualsysext.h

