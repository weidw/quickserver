#include "csourceclient.h"
#include <QHostAddress>
#include <QDataStream>

#include "../modbus/csinglesys.h"
#include "../modbus/cdualsys.h"
#include "../modbus/cdualsysext.h"

#include "../map/csourcemap.h"
#include "../map/cusermap.h"
#include "cuserclient.h"
#include <QDebug>

CSourceClient::CSourceClient(QObject* parent)
    : QTcpSocket(parent)
{
    connect(this,SIGNAL(readyRead()),this,SLOT(readClient()));
    connect(this,SIGNAL(disconnected()),this,SLOT(closeClient()));
    connect(this,SIGNAL(disconnected()),this,SLOT(deleteLater()));
    m_bState = false;
    m_nDeviID = 0;
    m_psys = NULL;
}

void CSourceClient::addObserver(Observer* o)
{
    if(o != NULL)
        m_list.push_back(o);
}

void CSourceClient::addObserver(const QString & ip)
{
    addObserver((Observer*)(CUserMap::getInstance()->find(ip)));
}

void CSourceClient::deleteObject(Observer* o)
{
    for(int i=0;i< (int)m_list.size();i++)
    {
        if(*m_list[i] == *o)
        {
            m_list.erase(m_list.begin() + i);
            return;
        }
    }
}

void CSourceClient::notifyObject(const QByteArray data)
{
    Observer*   b = NULL;
    CUserClient* c = NULL;

    if(m_bchange)
    {
        for(int i=0;i<(int)m_list.size();i++)
        {
            b = m_list[i];
            c = reinterpret_cast<CUserClient*>(b);
            if(m_name == c->m_strStatyLoad)
                c->update(data);
        }
    }
    m_bchange = false;
}

void CSourceClient::notifyObject(QString sourceName,modbusTypeEnum moubType,QString data)
{
    Observer*   b = NULL;
    CUserClient* c = NULL;

    if(m_bchange)
    {
        for(int i=0;i<(int)m_list.size();i++)
        {
            b = m_list[i];
            c = reinterpret_cast<CUserClient*>(b);
            if(m_name == c->m_strStatyLoad)
                c->update(sourceName,moubType,data);
        }
    }
    m_bchange = false;
}

void CSourceClient::notifyObject(std::queue<showbaseData> qdata,QString sourceName)
{
    Observer*   b = NULL;
    CUserClient* c = NULL;

    if(m_bchange)
    {
        for(int i=0;i<(int)m_list.size();i++)
        {
            b = m_list[i];
            c = reinterpret_cast<CUserClient*>(b);
            if(m_name == c->m_strStatyLoad)
                c->update(qdata,sourceName);
        }
    }
    m_bchange = false;
}

void CSourceClient::setChanged()
{
    m_bchange = true;
}

bool CSourceClient::ls(QStringList* pls)
{
    pls->clear();
    Observer*   b = NULL;
    CUserClient* c = NULL;

    for(int i=0;i<(int)m_list.size();i++)
    {
        b = m_list[i];
        c = reinterpret_cast<CUserClient*>(b);
        pls->push_back(c->m_name);
    }
    return true;
}

bool CSourceClient::sendSetLoc(modbusTypeEnum moubType, int nData)
{
    int nDS = 0;
    switch(moubType)
    {
     case E_STATE_ONOFF:   // 开关状态
        if(1 == nData)
            nData = 0xEF00;
        else
            nData = 0;
        nDS = 0x40;break;
     case E_STATE_MODE:    // 模式
        if(1 == nData)
            nData = 0xCC00;
        else
            nData = 0;
        nDS = 0x41;break;
     case E_STATE_FROST:   // 除霜状态
        if(1 == nData)
            nData = 0xEf00;
        else
            nData = 0;
        nDS = 0x4C;break;
    case E_STATE_PUMP:    // 水泵模式
       if(1 == nData)
           nData = 0x0001;
       else
           nData = 0;
       nDS = 0x4D;break;
     case E_TEMP_SETHEAT:  // 制热设定
        if(nData <20 || nData > 55)
            nData = nData > 40 ? 55:20;
        nData += 40;
        nDS = 0x42;break;
     case E_TEMP_SETCOOL:  // 制冷设定
        if(nData <7 || nData > 30)
            nData = nData > 52 ? 30:7;
        nData += 40;
        nDS = 0x43;break;
     case E_TEMP_DIFF:     // 温度回差
        if(nData <2 || nData > 7)
            nData = nData > 5 ? 7:2;

        nDS = 0x44;break;
     case E_TEMP_FROSTINTO:// 除霜进入温度
        if(nData <-9 || nData > 5)
            nData = nData > -3 ? 5:-9;
        nData += 40;
        nDS = 0x45;break;
     case E_TEMP_FROSROUTPUT:  // 除霜退出温度
        if(nData <-5 || nData > 15)
            nData = nData > 8 ? 15:-5;
        nData += 40;
        nDS = 0x46;break;
     case E_TIME_FROSTRUN:     // 除霜运行时间
        if(nData <2 || nData > 15)
            nData = nData > 10 ? 15:2;
        nDS = 0x47;break;
     case E_CYCLE_FROST:       // 除霜周期
        if(nData <10 || nData > 90)
            nData = nData > 60 ? 90:10;
        nDS = 0x48;break;
     case E_TIME_FROZENRUN:    // 防冻运行时间
        if(nData <1 || nData > 255)
            nData = nData > 30 ? 255:1;
        nDS = 0x49;break;
     case E_CYCLE_FROZEN:      // 防冻间隔
        if(nData <1 || nData > 255)
            nData = nData > 30 ? 255:1;

        nDS = 0x4A;break;
     case E_TEMP_PROTECTEXHAUST:// 排气保护温度
        if(nData <90 || nData > 120)
            nData = nData > 100 ? 120:90;
        nDS = 0x4B;break;
     default:return false;
    }
    write(m_psys->SendData(1,5,nDS,nData));
    return true;
}

 void CSourceClient::sendselSourceData()
 {
     write(m_psys->m_selAllData);
 }

void CSourceClient::addiniObserver()
{
    QString sql = "SELECT username as name FROM usergraph WHERE sourcename = '" + m_name + "'";
    dbOpt::getInstance()->selData(&m_dbList,sql);
    for(int i=0;i<(int)m_dbList.size();i++)
        addObserver(m_dbList[i]);
}

void CSourceClient::selSys()
{
    delete(m_psys);
    m_psys = NULL;
    switch (m_mydata.SysID.toInt()) {
    case 1:
        m_psys = new CSingleSys();
        break;
    case 2:
        m_psys = new CDualSys();
        break;
    case 3:
        m_psys = new CDualSysEx();
        break;
    default:
        m_psys = new CSingleSys();
        break;
    }
    m_psys->SendSelAllData();
}

void CSourceClient::readClient()
{
    if(!m_bState)
    {
        quint16 DeviId,CRC;
        QDataStream in(this);
        int  a = bytesAvailable();
        if(a == 4)
        {
            in >> DeviId >> CRC;
            if((DeviId + CRC) == 0xFFFF)
            {
                m_bState = true;
                m_nDeviID = DeviId;
                CSourceMap::getInstance()->addMapSock(QString::number(m_nDeviID),this);
                Subject::m_name = QString::number(m_nDeviID);
                addiniObserver();
                dbOpt::getInstance()->selData(m_mydata,"SELECT * FROM surceloctab WHERE modeid = '" + m_name + "'");
                selSys();
            }
        }
        else
           this->close();
        return ;
    }
    int nTemp  = m_psys->RecvData(this->readAll());

    if(source_recv_sellocdata == nTemp)
    {
        setChanged();
        notifyObject(m_name,m_psys->m_RecvSetType,"success");
    }
    else
    {
        setChanged();
        notifyObject(m_psys->getqData(),m_name);
    }
}

void CSourceClient::closeClient()
{
    CSourceMap::getInstance()->ersMapSock(QString::number(m_nDeviID));
}
