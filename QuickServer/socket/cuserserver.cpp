#include "cuserserver.h"
#include "cuserclient.h"

CuserServer::CuserServer(QObject* parent)
    :QTcpServer(parent)
{

}

CuserServer::~CuserServer()
{

}

void CuserServer::incomingConnection(int socketID)
{
    CUserClient* socket = new CUserClient(this);
    socket->setSocketDescriptor(socketID);
}
