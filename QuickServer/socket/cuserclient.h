#ifndef CUSERCLIENT_H
#define CUSERCLIENT_H

#include <QTcpSocket>
#include "../framework/observer.h"
#include <deque>
#include "analysisstreamreader.h"

class CUserClient: public QTcpSocket ,public Observer
{
    Q_OBJECT
public:
    CUserClient(QObject* parent = 0);

    void update(const QByteArray& data);
    void update (std::queue<showbaseData> qdata,QString sourceName);
    void update (QString sourceName,modbusTypeEnum moubType,QString data);
    QString  m_strStatyLoad;    // 停留点
protected:
    anaData login();
    void addiniObserver(const QString& ip);
    void delallObserver();
    void addObserver(const QString & ip);
    void delObserver(const QString & ip);
private slots:
    void readClient();
    void closeClient();
private:
    bool m_bState;              // 第一次进入标记
    QString  m_strDeviID;       // 登录名字
    bool m_bchange;
    std::deque<Observer>  m_list;
    std::deque<QString>   m_dbList;
    //XmlStreamReader       m_xml;
    AnalysisStreamReader* m_pxml;
};

#endif // CUSERCLIENT_H

