#include "csourceserver.h"
#include "csourceclient.h"

CSourceServer::CSourceServer(QObject* parent)
    :QTcpServer(parent)
{

}

CSourceServer::~CSourceServer()
{

}

void CSourceServer::incomingConnection(int socketID)
{
    CSourceClient* socket = new CSourceClient(this);
    socket->setSocketDescriptor(socketID);
}
