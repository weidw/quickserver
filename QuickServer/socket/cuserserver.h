#ifndef CUSERSERVER_H
#define CUSERSERVER_H

#include <QTcpServer>

class CuserServer : public QTcpServer
{
    Q_OBJECT
public:
    CuserServer(QObject* parent =0);
    ~CuserServer();
private:
    void incomingConnection(int socketID);

};

#endif // CUSERSERVER_H
