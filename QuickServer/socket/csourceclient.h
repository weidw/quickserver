#ifndef CMYCLIENT_H
#define CMYCLIENT_H

#include <QTcpSocket>
#include "../framework/subject.h"
#include <deque>
#include <list>
#include "cuserclient.h"
#include "../modbus/canalysis.h"
#include "../xml/xmlopt.h"
#include "../db/dbopt.h"

class CSourceClient : public QTcpSocket ,Subject
{
    Q_OBJECT
public:
    CSourceClient(QObject* parent = 0);
    void addObserver(Observer* o);
    void addObserver(const QString & ip);
    void deleteObject(Observer* o);
    void notifyObject(const QByteArray data);   // 发送数据
    void notifyObject(QString sourceName,modbusTypeEnum moubType,QString data); // 发送设置返回
    void notifyObject(std::queue<showbaseData> qdata,QString sourceName);       // 发送实时数据
    void setChanged();

    bool ls(QStringList* pls);
    CAnalysis*             m_psys;
    bool sendSetLoc(modbusTypeEnum moubType, int nData);
    void sendselSourceData();                   // 发送站点查询指令
protected:
    void addiniObserver();
    void selSys();
private slots:
    void readClient();
    void closeClient();
private:
    bool m_bState;  // 第一次进入标记
    int  m_nDeviID;
    bool m_bchange;

    sourceBaseData          m_mydata;
    std::deque<Observer*>   m_list;
    std::deque<QString>     m_dbList;
};

#endif // CMYCLIENT_H
