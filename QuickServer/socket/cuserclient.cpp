#include "cuserclient.h"
#include <QHostAddress>
#include <QDataStream>
#include "../map/cusermap.h"
#include "../map/csourcemap.h"
#include "csourceclient.h"
#include "../db/dbopt.h"
#include "../xml/xmlstreamreader.h"
#include "../xml/xmlopt.h"
#include "../json/jsonstreamreader.h"
#include "../json/jsonopt.h"
#include <QJsonDocument>


CUserClient::CUserClient(QObject* parent)
    : QTcpSocket(parent)
{
    connect(this,SIGNAL(readyRead()),this,SLOT(readClient()));
    connect(this,SIGNAL(disconnected()),this,SLOT(closeClient()));
    connect(this,SIGNAL(disconnected()),this,SLOT(deleteLater()));
    m_bState = false;
    m_strDeviID = "";
    m_strStatyLoad = "";
    m_pxml = NULL;
}

void CUserClient::update(const QByteArray& data)
{
    if(this->state() == QAbstractSocket::ConnectedState)
        qint64 a = this->write(data);
}

void CUserClient::update(std::queue<showbaseData> qdata,QString sourceName)
{
    if(NULL == m_pxml)
        return;
    if(this->state() == QAbstractSocket::ConnectedState)
        qint64 a = this->write(m_pxml->notify(qdata,sourceName));
}

void CUserClient::update(QString sourceName,modbusTypeEnum moubType,QString data)
{
    if(NULL == m_pxml)
        return;
    if(this->state() == QAbstractSocket::ConnectedState)
        qint64 a = this->write(m_pxml->notify(sourceName,moubType,data));
}

void CUserClient::addObserver(const QString & ip)
{
    CSourceClient*  s = (CSourceClient*)CSourceMap::getInstance()->find(ip);
    if(s!= NULL)
        s->addObserver((Observer*)CUserMap::getInstance()->find(m_name));
}

void CUserClient::delObserver(const QString & ip)
{
    CSourceClient*  s = (CSourceClient*)CSourceMap::getInstance()->find(ip);
    if(s!= NULL)
        s->deleteObject((Observer*)CUserMap::getInstance()->find(m_name));
}

void CUserClient::addiniObserver(const QString& ip)
{
    m_bState = true;
    m_strDeviID = ip;
    CUserMap::getInstance()->addMapSock(ip,this);
    Observer::m_name = ip;

    QString sql = "SELECT sourcename as name FROM usergraph WHERE username = '" + m_name + "'";
    dbOpt::getInstance()->selData(&m_dbList,sql);

    for(int i=0;i<(int)m_dbList.size();i++)
        addObserver(m_dbList[i]);
    if(m_dbList.size() > 0)
        m_strStatyLoad = m_dbList[0];
}

void CUserClient::delallObserver()
{
    QString sql = "SELECT sourcename as name FROM usergraph WHERE username = '" + m_name + "'";
    dbOpt::getInstance()->selData(&m_dbList,sql);
    for(int i=0;i<(int)m_dbList.size();i++)
        delObserver(m_dbList[i]);
    CUserMap::getInstance()->ersMapSock(m_strDeviID);
    m_bState = false;
    m_strDeviID = "";
    Observer::m_name = "";
}

anaData CUserClient::login()
{
    QString json = readAll();
    QJsonParseError error;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toUtf8(),&error);
    if (error.error == QJsonParseError::NoError) {
        if (jsonDocument.isObject()) {
            QVariantMap result = jsonDocument.toVariant().toMap();
            QVariantMap::const_iterator it = result.find("login");
            if(it != result.end())
            {
                loginJson login;
                QByteArray strArray;
                QString strName = login.analysis(result["login"].toMap());
                if(login.m_strSYS == "xml")
                {
                    m_pxml = new XmlStreamReader();
                    loginXML xmllogin;
                    xmllogin.m_strName = login.m_strName;
                    strArray = xmllogin.notice();
                }
                else
                {
                    m_pxml = new JsonStreamReader();
                    strArray = login.notice();
                }
                return anaData("login",strName,strArray);
            }
            else
                return anaData();
        }
    } else {
        qDebug() << error.errorString();
        return anaData();
    }
    return anaData();
}

void CUserClient::readClient()
{
    if(!m_bState)
    {
        anaData a = login();
        if(!a.elementName.isEmpty())
        {
            addiniObserver(a.elementName);
            write(a.returnData);
        }
    }
    else
    {
        if(m_pxml == NULL)
            return;
        anaData stream = m_pxml->operatorFunc(this);
        if("real" == stream.elementType)
            m_strStatyLoad = stream.elementName;
        if(!stream.elementName.isEmpty())
            write(stream.returnData);
        if(stream.elementType == "exit" &&
                m_strDeviID == stream.elementName)
        {
            delallObserver();
            delete m_pxml;
            m_pxml = NULL;
        }
    }
}

void CUserClient::closeClient()
{
    delete m_pxml;
    m_pxml = NULL;
    delallObserver();
}
