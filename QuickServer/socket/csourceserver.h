#ifndef CMYTCPSERVER_H
#define CMYTCPSERVER_H

#include <QTcpServer>

class CSourceServer : public QTcpServer
{
    Q_OBJECT
public:
    CSourceServer(QObject* parent =0);
    ~CSourceServer();
private:
    void incomingConnection(int socketID);

};

#endif // CMYTCPSERVER_H
