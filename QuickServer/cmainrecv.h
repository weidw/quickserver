#ifndef CMAINRECV_H
#define CMAINRECV_H

#include <QObject>
#include <QTimer>

class CMainRecv : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList ls   READ ls     NOTIFY lsChanged)
public:
    static CMainRecv* getInstance();
    QStringList ls(){return m_ls;}
    Q_INVOKABLE void        clickSourceData(int nIndex);
signals:
    void lsChanged();
public slots:
    void onTimer();
private:
    explicit CMainRecv(QObject *parent = 0);
    QStringList         m_ls;
    static CMainRecv*   s_recv;
    QTimer               m_timer;
};

#endif // CMAINRECV_H
