#ifndef ANALYSISSTREAMREADER_H
#define ANALYSISSTREAMREADER_H

#include "afxpublic.h"
#include <QIODevice>
#include "afxpublic.h"
#include <queue>

class AnalysisStreamReader
{
public:
    AnalysisStreamReader();
    virtual void setopt(QString elementName);
    virtual anaData specifuFunc(QIODevice* device);

    virtual anaData operatorFunc(QIODevice* device);
    virtual anaData operatorFunc(QIODevice* device,QString elementName);

    virtual QByteArray notify (std::queue<showbaseData> qdata,QString sourceName);
    virtual QByteArray notify (QString sourceName,modbusTypeEnum moubType,QString data);
public:
    QString m_sysType;
};

#endif // ANALYSISSTREAMREADER_H
