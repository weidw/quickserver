#ifndef DBOPT_H
#define DBOPT_H

#include <QObject>
#include <QSqlDatabase>
#include <QMutexLocker>
#include "../ini/config.h"
#include <deque>
#include <queue>
#include "../afxpublic.h"

enum DB_OPT{enm_userLoc =1,enm_hisDat,enm_Chart};

class sourceBaseData
{
public:
    sourceBaseData()
    {
        ModeID      = "";
        DeviceID    = "";
        SysID       = "";
        Name        = "";
    }
    sourceBaseData(QString mode,QString device,QString sys,QString name):
        ModeID(mode),DeviceID(device),SysID(sys),Name(name){}
    QString     ModeID;
    QString     DeviceID;
    QString     SysID;
    QString     Name;
};

class dbOpt
{
private:
    dbOpt();
public:
    static dbOpt* getInstance();
    static void SetSqlData(const DBIni dbIni);
    static bool InsLog(QString strOpt,QString strCom,QString strName);
    bool CreateCon();
    bool checkPwd(QString strSql);
    bool InsertLog(QString strOpt,QString strCom,QString strName);
    bool queryCheck(QSqlQuery* query);
    int  selData(QString sql = ""); // 返回条数
    bool selData(QList<QObject*> * pLs,QString strCondition = "",int nflog = 1); // nflog ==1 为selLog nFlog == 2 位selIntOut
    bool selData(std::queue<showbaseData>* pls,QString SQL);
    int  selData(std::deque<QString>* pLs,QString strCondition = ""); // 返回name 的 list 列表
    bool selData(sourceBaseData& source,QString SQL);

    int  selChartDate(QList<QObject*> * pLs,QString strCondition,QDate date); // 图表查询数据用

    bool upData(QString strSql);
    bool userInto(int nLoc);
    bool userOut(int nLoc);
    bool insertData(QString strSql);
private:
    QSqlDatabase m_db;
    static dbOpt*  m_dbopt;
    static QString s_strHostName;
    static QString s_strDataBaseName;
    static QString s_strUserName;
    static QString s_strPwd;
    QMutex  mutex;
};

#endif // DBOPT_H
