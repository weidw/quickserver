#include "dbopt.h"
#include <QStringList>
#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QDate>

dbOpt*  dbOpt::m_dbopt = NULL;
QString dbOpt::s_strHostName = "";
QString dbOpt::s_strDataBaseName = "";
QString dbOpt::s_strUserName = "";
QString dbOpt::s_strPwd = "";

dbOpt::dbOpt()
{

}

dbOpt* dbOpt::getInstance()
{

    if(m_dbopt == NULL)
    {
        m_dbopt = new dbOpt();
        SetSqlData(Config::getInstance()->dbIni());
        m_dbopt->CreateCon();
    }
    return m_dbopt;
}
void dbOpt::SetSqlData(const DBIni dbIni)
{
    s_strHostName       = dbIni.hostName;
    s_strDataBaseName   = dbIni.dbName;
    s_strUserName       = dbIni.userName;
    s_strPwd            = dbIni.pwd;
}

bool dbOpt::InsLog(QString strOpt,QString strCom,QString strName)
{
    return getInstance()->InsertLog(strOpt,strCom,strName);
}

bool dbOpt::CreateCon()
{
#ifdef QT_DEBUG
    QStringList drivers = QSqlDatabase::drivers();
    qDebug() << drivers;
#endif
    m_db.close();
    m_db = QSqlDatabase::addDatabase("QMYSQL");
    m_db.setHostName(s_strHostName); //"localhost"192.168.1.171 58.58.135.78 SKY-20160511XFS
    m_db.setDatabaseName(s_strDataBaseName);
    m_db.setPort(3306);
    m_db.setUserName(s_strUserName);
    m_db.setPassword(s_strPwd);

    if(!m_db.open())
    {
 //       CMyDebug::getInstance()->record("db connect fail.");
 //       CMyDebug::getInstance()->record(m_db.lastError().text());
#ifdef QT_DEBUG
        qDebug() << "db connect fail.";
        qDebug() << m_db.lastError().text();
#endif
        return false;
    }
#ifdef QT_DEBUG
    qDebug() << "db connect success.";
#endif
//    CMyDebug::getInstance()->record("db connect success.");
    return true;
}

bool dbOpt::checkPwd(QString strSql)
{
    QMutexLocker locker(&mutex);
 //   if(CCurrUserData::getInstance()->wraing() == ""||
 //           CCurrUserData::getInstance()->wraing() == "输入的用户名或密码不存在！")
 //   {}
 //   else
  //      return false;
    QSqlQuery query(m_db);
    query.prepare(strSql);
    query.exec();
    if(!queryCheck(&query))
        return false;
    bool bState = false;
    while(query.next())
    {
        bState = true;
    }
    query.clear();
 //   if(CCurrUserData::getInstance()->wraing() == "")
 //       CCurrUserData::getInstance()->setWraing("输入的用户名或密码不存在！");
    return bState;
}

bool dbOpt::InsertLog(QString strOpt,QString strCom,QString strName)
{
    QMutexLocker locker(&mutex);
    QSqlQuery query(m_db);
    query.prepare("INSERT INTO user_opt_log (user_name,log_opt,log_acs,log_time,log_comment)"
                  "VALUES (:p_name,:p_opt,1,NOW(),:p_comment)");
    query.bindValue(":p_name",strName);
    query.bindValue(":p_opt",strOpt);
    query.bindValue(":p_comment",strCom);
    query.exec();

    return queryCheck(&query);
}

 bool dbOpt::queryCheck(QSqlQuery* query)
 {
     if(query == NULL)
         return false;
     if(!query->isActive())
     {
//         CMyDebug::getInstance()->record("query sql error:" + query->lastError().text());
         if(m_db.isValid())
             CreateCon();
#ifdef QT_DEBUG
        qDebug() << "query sql error:" << query->lastError().text()
                << " Type:" << query->lastError().type()
                << " Number:" << query->lastError().number();
#endif
        return false;
     }
     return true;
 }

 int dbOpt::selData(QString sql)
 {
     QMutexLocker locker(&mutex);
     QSqlQuery query(m_db);

     query.prepare(sql);
     query.exec();
     if(!queryCheck(&query))
     {
         query.clear();
         return 0;
     }
     int ncount = 0;
     while(query.next())
     {
        ncount = query.value(0).toInt();
     }
     query.clear();
     return ncount;
 }

 bool dbOpt::selData(QList<QObject*> * pLs,QString strCondition,int nflog)
 {
     QMutexLocker locker(&mutex);
     if(pLs == NULL)
            return false;
     pLs->clear();
     QSqlQuery query(m_db);
     query.prepare(strCondition);
     query.exec();
     if(!queryCheck(&query))
     {
        // query.clear();
         return false;
     }
    switch (nflog) {
    case enm_userLoc:
    {
    }
        break;
    case enm_hisDat:
    {
    }
        break;
    case enm_Chart:
    {
    }
        break;
    default:
        return false;
    }
    query.clear();
    return true;
 }

 bool dbOpt::selData(std::queue<showbaseData>* pls,QString SQL)
 {
    QMutexLocker locker(&mutex);
    if(pls == NULL)
           return false;
    while(pls->size())
        pls->pop();

    QSqlQuery query(m_db);
    query.prepare(SQL);
    query.exec();
    if(!queryCheck(&query))
        return false;
    while(query.next())
        pls->push(showbaseData(query.value("name").toString(),
                                   query.value("loc").toString()));
    query.clear();
    return true;
 }

 int dbOpt::selData(std::deque<QString>* pLs,QString strCondition)
 {
     QMutexLocker locker(&mutex);
     if(pLs == NULL)
            return false;
     pLs->clear();
     QSqlQuery query(m_db);
     query.prepare(strCondition);
     query.exec();
     if(!queryCheck(&query))
     {
        // query.clear();
         return -1;
     }
     while(query.next())
         pLs->push_back(query.value("name").toString());
     query.clear();
    return pLs->size();
 }

 bool dbOpt::selData(sourceBaseData& source,QString SQL)
 {
     QMutexLocker locker(&mutex);

     QSqlQuery query(m_db);
     query.prepare(SQL);
     query.exec();
     if(!queryCheck(&query))
     {
        // query.clear();
         return false;
     }
     while(query.next())
     {
        source.ModeID   = query.value("modeid").toString();
        source.DeviceID = query.value("deviceid").toString();
        source.SysID    = query.value("sysid").toString();
        source.Name     = query.value("name").toString();
     }
     query.clear();
    return true;
 }

int dbOpt::selChartDate(QList<QObject*> * pLs,QString strCondition,QDate date)
{

    QMutexLocker locker(&mutex);
    if(pLs == NULL)
           return false;
    pLs->clear();
    QSqlQuery query(m_db);

    query.prepare(strCondition);
    query.bindValue(":p_date",date);
    query.exec();
    if(!queryCheck(&query))
    {
        query.clear();
        return 0;
    }
    while(query.next())
    {
        /*pLs->push_back(new chartBaseDate(query.value("time").toInt(),
                                          query.value("value").toInt()));*/
    }
   query.clear();
   return 1;
}

 bool dbOpt::upData(QString strSql)
 {
     QMutexLocker locker(&mutex);
     QSqlQuery query(m_db);
     query.prepare(strSql);
     query.exec();

     return queryCheck(&query);
 }

 bool dbOpt::userInto(int nLoc)
 {
    QMutexLocker locker(&mutex);
    QSqlQuery query(m_db);
    query.prepare("UPDATE loc_tab SET loc_state = 1,loc_ontime = NOW() WHERE loc_id = :loc_id");
    query.bindValue(":loc_id",nLoc);
    query.exec();

    return queryCheck(&query);
 }

 bool dbOpt::userOut(int nLoc)
 {
     QMutexLocker locker(&mutex);
     QSqlQuery query(m_db);
     query.prepare("UPDATE loc_tab SET loc_state = 0,loc_outtime = NOW() WHERE loc_id = :loc_id");
     query.bindValue(":loc_id",nLoc);
     query.exec();

     return queryCheck(&query);
 }

 bool dbOpt::insertData(QString strSql)
 {
     QMutexLocker locker(&mutex);
     QSqlQuery query(m_db);
     query.prepare(strSql);
     query.exec();

     return queryCheck(&query);
 }
