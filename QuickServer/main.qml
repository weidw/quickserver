import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Controls 1.2

Window {
    width: 600
    height: 750
    visible: true

    TableView {
        id: gridView1
        x: 17
        y: 472
        width: 567
        height: 260
        TableViewColumn {
                role: "title"
                title: "名称"
                width: gridView1.width/6-1
        }
        TableViewColumn {
            role: "author"
            title: "数值"
            width: gridView1.width/3-1
        }

        TableViewColumn {
            role: "title"
            title: "名称"
            width: gridView1.width/6-1
        }
        TableViewColumn {
            role: "author"
            title: "数值"
            width: gridView1.width/3-1
        }
        model:  userSocket.ls
    }

    ComboBox {
        id: comboBox2
        x: 306
        y: 145
        width: 160
        height: 20
        model:  userSocket.ls
    }

    TableView {
        id: gridView2
        x: 17
        y: 171
        width: 276
        height: 260
        TableViewColumn {
            width: 90
            title: "名称"
            role: "title"
        }

        TableViewColumn {
            width: 180
            title: "数值"
            role: "author"
        }
        model: scourceSocket.ls
        onDoubleClicked: myRecv.clickSourceData(scourceSocket.ls[row])
    }

    TableView {
        id: gridView3
        x: 306
        y: 171
        width: 278
        height: 260
        TableViewColumn {
            width: 90
            title: "名称"
            role: "title"
        }

        TableViewColumn {
            width: 180
            title: "数值"
            role: "author"
        }
        model: userSocket.ls
    }

    TableView {
        id: gridView4
        x: 17
        y: 24
        width: 276
        height: 110
        TableViewColumn {
            width: 90
            title: "名称"
            role: "title"
        }

        TableViewColumn {
            width: 180
            title: "数值"
            role: "author"
        }
        model: myRecv.ls
    }
}
