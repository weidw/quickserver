#ifndef CONFIG_H
#define CONFIG_H
#include <QString>
#include <map>
#include <list>
#include <QPoint>

typedef struct SockIni_{
    QString ip;
    int    port;
}SockeIni;

typedef struct DBIni_{
    QString hostName;
    QString dbName;
    QString userName;
    QString pwd;
}DBIni;

class initshowData
{
public:
    initshowData()
    {
        ElemnetName = "";
        point = QPoint();
    }
    initshowData(QString str,QPoint p):ElemnetName(str),point(p){}
    QString ElemnetName;
    QPoint  point;
};

struct node{
 bool operator()(const initshowData& t1,const initshowData& t2){
  //return t1<t2;    //会产生升序排序,若改为>,则变为降序
    if(t1.point.x() < t2.point.x())
        return true;
    else if(t1.point.x() == t2.point.x())
        return t1.point.y() < t2.point.y();
    else
        return false;
 }
};

class Config
{
public:
    static Config* getInstance();

    int         userIni(){return m_UserIni;}
    SockeIni    socketIni(){return m_socketIni;}
    DBIni       dbIni(){return m_dbIni;}
    QString     secretkey(){return m_secretkey;}
    void        setsecretkey(QString str);

    void        saveKey(std::map<QString,QPoint>& pMap);
    void        readKey(std::list<initshowData>& list);
private:
    Config();
    static Config* s_pconfig;
    int         m_UserIni;            // 初始化信息
    SockeIni    m_socketIni;          // sock 连接信息
    DBIni       m_dbIni;              // 数据库连接信息
    QString     m_secretkey;          // 用户注册码
};

#endif // CONFIG_H
