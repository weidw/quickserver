#include "config.h"
#include <QSettings>

Config* Config::s_pconfig = NULL;

Config::Config()
{
    QSettings *configIniRead = new QSettings("config.ini", QSettings::IniFormat);
    if(configIniRead == NULL)
        m_UserIni = 0;
    else
    {
        m_UserIni         = configIniRead->value("/config/UseIni" ,0).toInt();

        m_socketIni.ip    = configIniRead->value("/IP/ip"         ,"192.168.1.2").toString();
        m_socketIni.port  = configIniRead->value("/IP/port"       ,5000).toInt();

        m_dbIni.hostName  = configIniRead->value("/db/Host"       ,"127.0.0.1").toString();
        m_dbIni.dbName    = configIniRead->value("/db/DataBaseName","quickser").toString();
        m_dbIni.userName  = configIniRead->value("/db/userName"   ,"root").toString();
        m_dbIni.pwd       = configIniRead->value("/db/pwd"        ,"root").toString();

        m_secretkey       = configIniRead->value("/secret/Key"    ,"1-1").toString();
        //读入入完成后删除指针
        delete configIniRead;
    }
    if(m_UserIni == 0)
    {
        m_socketIni.ip    = "192.168.1.2";
        m_socketIni.port  = 5000;

        m_dbIni.hostName  = "127.0.0.1";
        m_dbIni.dbName    = "quickser";
        m_dbIni.userName  = "root";
        m_dbIni.pwd       = "root";
        m_secretkey       = "1-1";
    }
}

Config* Config::getInstance()
{
    if(s_pconfig == NULL)
        s_pconfig = new Config();
    return s_pconfig;
}

void Config::setsecretkey(QString str)
{
     QSettings *configIniRead = new QSettings("config.ini", QSettings::IniFormat);
     if(configIniRead != NULL)
     {
         configIniRead->setValue("/secret/Key",str);
         //读入入完成后删除指针
         delete configIniRead;
     }
}

void Config::saveKey(std::map<QString,QPoint>& pMap)
{
    QSettings *configIniRead = new QSettings("config.ini", QSettings::IniFormat);
    if(configIniRead != NULL)
    {
        configIniRead->remove("Key");
        configIniRead->beginGroup("Key");
        std::map<QString,QPoint>::iterator it = pMap.begin();
        while(it != pMap.end())
        {
            configIniRead->setValue(it->first,it->second);
            it++;
        }
        delete configIniRead;
    }
}

void Config::readKey(std::list<initshowData>& list)
{
    QSettings *configIniRead = new QSettings("config.ini", QSettings::IniFormat);
    if(configIniRead != NULL)
    {
        list.clear();
        configIniRead->beginGroup("Key");
        QStringList strList = configIniRead->allKeys();
        for (int i = 0; i < strList.size(); ++i)
            list.push_back(initshowData(strList.at(i),
                                        configIniRead->value(strList.at(i)).toPoint())
                            );
        delete configIniRead;
        list.sort(node());
    }
}

