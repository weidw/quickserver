#ifndef XMLSTREAMREADER_H
#define XMLSTREAMREADER_H
#include <QXmlStreamReader>
#include <deque>
#include <map>
#include "xmloperator.h"
#include "../afxpublic.h"
#include "../analysisstreamreader.h"

class XmlStreamReader : public AnalysisStreamReader
{
public:
    XmlStreamReader();

    void setopt(QString elementName);
    anaData specifuFunc(QIODevice* device);

    anaData operatorFunc(QIODevice* device);
    anaData operatorFunc(QIODevice* device,QString elementName);
    QByteArray notify (std::queue<showbaseData> qdata,QString sourceName);
    QByteArray notify (QString sourceName,modbusTypeEnum moubType,QString data);

    bool readFile(const QString &filename);
private:
    XmlOperator* find(const QString& Ip);
    QXmlStreamReader    reader;
    std::map<QString,XmlOperator*>   m_map;
    XmlOperator*    m_pOperator;
    QString     m_elementName;
};

#endif // XMLSTREAMREADER_H
