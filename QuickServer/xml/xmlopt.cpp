#include "xmlopt.h"
#include "../db/dbopt.h"
#include "../afxpublic.h"
#include "../socket/csourceclient.h"
#include <QDateTime>

loginXML::loginXML()
{
    m_strName = "";
}

QString loginXML::analysis(QXmlStreamReader* xmlreader)
{
    /********************
    *   <login>	// 登录
    *       <username> admin </username>
    *       <pwd> admin </pwd>
    *       <sys> xml </sys>
    *   </login>
    ********************/
    QString strpwd = "";
    QString strTemp = "";

    while(!xmlreader->atEnd())
    {
        if(xmlreader->isStartElement())
        {
            strTemp = xmlreader->name().toString();
            if("username" == strTemp)
            {
                m_strName = xmlreader->readElementText();
                xmlreader->readNext();
            }
            else if("pwd" == strTemp)
            {
                strpwd = xmlreader->readElementText();
                xmlreader->readNext();
            }
            else
                xmlreader->readNext();
        }
        else
            xmlreader->readNext();
    }
    return m_strName;
}

QByteArray loginXML::notice()
{
    QByteArray output;
    QXmlStreamWriter xml(&output);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();
    xml.writeStartElement("login");

    xml.writeTextElement("username",m_strName);
    xml.writeTextElement("limit","100");
    xml.writeTextElement("explain","success");

    xml.writeEndElement();
    xml.writeEndDocument();

    return output;
}

exitXML::exitXML()
{

}

QString exitXML::analysis(QXmlStreamReader* xmlreader)
{
    /********************
    *   <exit>	// 注销
    *       <username> admin </username>
    *   </exit>
    ********************/
    QString strname = "";
    QString strTemp = "";

    while(!xmlreader->atEnd())
    {
        if(xmlreader->isStartElement())
        {
            strTemp = xmlreader->name().toString();
            if("username" == strTemp)
            {
                strname = xmlreader->readElementText();
                xmlreader->readNext();
            }
            else
                xmlreader->readNext();
        }
        else
            xmlreader->readNext();
    }
    return strname;
}

QByteArray exitXML::notice()
{
    QByteArray output;
    QXmlStreamWriter xml(&output);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();
    xml.writeStartElement("exit");
    xml.writeTextElement("username","admin");
    xml.writeTextElement("explain","success");
 //   xml.writeAttribute("username","admin");
 //   xml.writeAttribute("explain","success");
    xml.writeEndElement();
    xml.writeEndDocument();

    return output;
}

realXML::realXML()
{
    m_strSource = "";
}

QString realXML::analysis(QXmlStreamReader* xmlreader)
{
     /********************
      * <real>
      *     <loc>1</loc>
      * </real>
     ********************/

    xmlreader->readNext();
    while(!xmlreader->atEnd())
    {
        if(xmlreader->isStartElement())
        {
            if("loc" == xmlreader->name())
            {
                m_strSource = xmlreader->readElementText();
                xmlreader->readNext();
            }
            else
                    xmlreader->readNext();
        }
        else
            xmlreader->readNext();
    }
    return m_strSource;
}

QByteArray realXML::notice()
{
    CSourceClient* s = (CSourceClient*)CSourceMap::getInstance()->find(m_strSource);
    if(NULL != s)
        return notice(s->m_psys->getqData(),m_strSource);
    QByteArray output;
    QXmlStreamWriter xml(&output);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();
    xml.writeStartElement("real");
    xml.writeAttribute("location_name",m_strSource);
    xml.writeAttribute("date",QDateTime::currentDateTime().toString("yyyy-MM-dd hh::mm::ss"));
    xml.writeAttribute("notify","对应源站点未登录");
    xml.writeEndElement();
    xml.writeEndDocument();

    return output;
}

QByteArray realXML::notice(std::queue<showbaseData> qdata,QString sourceName)
{
    QByteArray output;
    QXmlStreamWriter xml(&output);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();
    xml.writeStartElement("real");
    xml.writeAttribute("location_name",sourceName);
    xml.writeAttribute("date",QDateTime::currentDateTime().toString("yyyy-MM-dd hh::mm::ss"));
    xml.writeAttribute("notify","");

    while(qdata.size())
    {
        xml.writeTextElement(qdata.front().elementName,qdata.front().strData);
        qdata.pop();
    }
    xml.writeEndElement();
    xml.writeEndDocument();

    return output;
}

locXML::locXML()
{
    m_strName = "";
}

QString locXML::analysis(QXmlStreamReader* xmlreader)
{
    /********************
     * <?xml version = "1.0" ?>
     * <location>
     *     <username>admin</username>
     * </location>
    ********************/

   xmlreader->readNext();
   while(!xmlreader->atEnd())
   {
       if(xmlreader->isStartElement())
       {
           if("username" == xmlreader->name())
           {
               m_strName = xmlreader->readElementText();
               xmlreader->readNext();
           }
           else
                   xmlreader->readNext();
       }
       else
           xmlreader->readNext();
   }
   return m_strName;
}

QByteArray locXML::notice()
{
    /********************
     * <?xml version = "1.0" ?>
     * <location>
     *     <loc name = "华旗新能源">1</loc>
     *     <loc name = "华福新能源">2</loc>
     *     <loc name = "华慧新能源">3</loc>
     * </location>
    ********************/

    QByteArray output;
    QXmlStreamWriter xml(&output);
    std::queue<showbaseData> qdata;
    QString sql = "SELECT sourcename AS loc,name FROM usergraph "
                  "LEFT JOIN surceloctab ON sourcename=modeid WHERE username = '" +
                  m_strName + "'";
    xml.setAutoFormatting(true);
    xml.writeStartDocument();
    xml.writeStartElement("location");
    dbOpt::getInstance()->selData(&qdata,sql);
    while(qdata.size())
    {
        xml.writeStartElement("loc");
        xml.writeAttribute("name", qdata.front().elementName);
        xml.writeCharacters(qdata.front().strData);
        xml.writeEndElement();
        qdata.pop();
    }
    xml.writeEndElement();
    xml.writeEndDocument();

    return output;
}

setLocXML::setLocXML()
{}

QString setLocXML::analysis(QXmlStreamReader* xmlreader)
{
    /********************
     * <?xml version = "1.0" ?>
     * <setloc location_name = "1">
     *     <state_onoff>1</state_onoff>
     * </setloc>
    ********************/
   QString strLocName = xmlreader->attributes().value("location_name").toString();
   xmlreader->readNext();

   CSourceClient* s = (CSourceClient*)CSourceMap::getInstance()->find(strLocName);
   if(NULL == s)
       return "";

   while(!xmlreader->atEnd())
   {
       if(xmlreader->isStartElement())
       {
           modbusTypeEnum moubType =  elementTypeName::getInstance()->selElementName(xmlreader->name().toString());
           QString strTemp = xmlreader->readElementText();
           s->sendSetLoc(moubType,strTemp.toInt());  // 发送
           xmlreader->readNext();
       }
       else
           xmlreader->readNext();
   }
   return strLocName;
}

QByteArray setLocXML::notice()
{
    return QByteArray();
}

QByteArray setLocXML::notice(QString sourceName,modbusTypeEnum moubType,QString data)
{
    /********************
     * <?xml version = "1.0" ?>
     * <setloc location_name = "1">
     *     <state_onoff>success</state_onoff>
     * </setloc>
    ********************/
    QByteArray output;
    QXmlStreamWriter xml(&output);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();
    xml.writeStartElement("setloc");
    xml.writeAttribute("location_name",sourceName);

    xml.writeTextElement(elementTypeName::getInstance()->selElementName(moubType),data);

    xml.writeEndElement();
    xml.writeEndDocument();

    return output;
}

noticeXML::noticeXML()
{}

QString noticeXML::analysis(QXmlStreamReader* xmlreader)
{
    return "";
}
QByteArray noticeXML::notice()
{
    return QByteArray();
}

QByteArray noticeXML::notice(QString data)
{
    /********************
     * <?xml version = "1.0" ?>
     * <notice>
     *     <message>success</message>
     * </notice>
    ********************/
    QByteArray output;
    QXmlStreamWriter xml(&output);
    xml.setAutoFormatting(true);
    xml.writeStartDocument();
    xml.writeStartElement("notice");
    xml.writeTextElement("message",data);
    xml.writeEndElement();
    xml.writeEndDocument();
    return output;
}

