#ifndef LOGINXML_H
#define LOGINXML_H

#include "xmloperator.h"
#include "../modbus/csinglesys.h"

class loginXML : public XmlOperator
{
public:
    loginXML();
    QString analysis(QXmlStreamReader* xmlreader);
    QByteArray notice();
    QString     m_strName;
};

class exitXML : public XmlOperator
{
public:
    exitXML();
    QString analysis(QXmlStreamReader* xmlreader);
    QByteArray notice();
};

class realXML : public XmlOperator
{
public:
    realXML();
    QString analysis(QXmlStreamReader* xmlreader);
    QByteArray notice();
    QByteArray notice(std::queue<showbaseData> qdata,QString sourceName);
    QString m_strSource;
};

class locXML : public XmlOperator
{
public:
    locXML();
    QString analysis(QXmlStreamReader* xmlreader);
    QByteArray notice();
    QString m_strName;
};

class setLocXML : public XmlOperator
{
public:
    setLocXML();
    QString analysis(QXmlStreamReader* xmlreader);
    QByteArray notice();
    QByteArray notice(QString sourceName,modbusTypeEnum moubType,QString data);
};

class noticeXML : public XmlOperator
{
public:
    noticeXML();
    QString analysis(QXmlStreamReader* xmlreader);
    QByteArray notice();
    QByteArray notice(QString data);
};

#endif // LOGINXML_H
