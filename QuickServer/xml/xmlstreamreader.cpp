#include "xmlstreamreader.h"
#include <QFile>
#include <QDebug>
#include "xmlopt.h"

XmlStreamReader::XmlStreamReader()
{
    m_elementName = "";
    m_map.insert(std::map<QString,XmlOperator*>::value_type("login"     , new loginXML())   );  // 登录
    m_map.insert(std::map<QString,XmlOperator*>::value_type("exit"      , new exitXML())    );  // 退出
    m_map.insert(std::map<QString,XmlOperator*>::value_type("real"      , new realXML())    );  // 实时
    m_map.insert(std::map<QString,XmlOperator*>::value_type("location"  , new locXML())     );  // 站点
    m_map.insert(std::map<QString,XmlOperator*>::value_type("setloc"    , new setLocXML())  );  // 设置
}
void XmlStreamReader::setopt(QString elementName)
{
    std::map<QString,XmlOperator*>::const_iterator it = m_map.find(elementName);
    if(it != m_map.end())
    {
        m_pOperator = it->second;
        m_elementName = elementName;
    }
}

anaData XmlStreamReader::specifuFunc(QIODevice* device)
{
    QXmlStreamReader xmlreader;
    xmlreader.setDevice(device);
    QString a = xmlreader.name().toString();
    xmlreader.readNext();
    while(!xmlreader.atEnd())
    {
        if(xmlreader.isStartElement())
        {
            a = xmlreader.name().toString();
            if(a == m_elementName)
                if(NULL == m_pOperator)
                    return anaData();
                else
                {
                    QString strName = m_pOperator->analysis(&xmlreader);
                    return anaData(a,strName,
                                      m_pOperator->notice());
                }
            else
                xmlreader.readNext();
        }
        else
            xmlreader.readNext();
    }
    if(xmlreader.hasError())
    {
        qDebug() << xmlreader.errorString();
    }
    return anaData();
}

anaData XmlStreamReader::operatorFunc(QIODevice* device)
{
    QXmlStreamReader xmlreader;
    xmlreader.setDevice(device);
    QString a = xmlreader.name().toString();
    xmlreader.readNext();
    while(!xmlreader.atEnd())
    {
        if(xmlreader.isStartElement())
        {
            a = xmlreader.name().toString();
            std::map<QString,XmlOperator*>::const_iterator it = m_map.find(a);
            if(it != m_map.end())
            {
                QString strName = it->second->analysis(&xmlreader);
                return anaData(a,strName,
                                  it->second->notice());
            }
            xmlreader.readNext();
        }
        else
            xmlreader.readNext();
    }
    if(xmlreader.hasError())
    {
        qDebug() << xmlreader.errorString();
    }
    return anaData();
}

anaData XmlStreamReader::operatorFunc(QIODevice* device,QString elementName)
{
    QXmlStreamReader xmlreader;
    xmlreader.setDevice(device);
    QString a = xmlreader.name().toString();
    xmlreader.readNext();
    while(!xmlreader.atEnd())
    {
        if(xmlreader.isStartElement())
        {
            a = xmlreader.name().toString();
            if(a == elementName)
            {
                XmlOperator* px = find(elementName);
                if(NULL != px)
                {
                    QString strName = px->analysis(&xmlreader);
                    return anaData(elementName,strName,
                                      px->notice());
                }
                else
                    xmlreader.readNext();
            }
        }
        else
            xmlreader.readNext();
    }
    if(xmlreader.hasError())
    {
        qDebug() << xmlreader.errorString();
    }
    return anaData();
}

QByteArray XmlStreamReader::notify (std::queue<showbaseData> qdata,QString sourceName)
{
    realXML real;
    return real.notice(qdata,sourceName);
}

QByteArray XmlStreamReader::notify (QString sourceName,modbusTypeEnum moubType,QString data)
{
    setLocXML setloc;
    return setloc.notice(sourceName,moubType,data);
}

bool XmlStreamReader::readFile(const QString &filename)
{
    QFile file(filename);
    if(!file.open(QFile::ReadOnly | QFile::Text))
    {
        return false;
    }
    reader.setDevice(&file);
    QString a = reader.name().toString();
    reader.readNext();
    while(!reader.atEnd())
    {
        if(reader.isStartElement())
        {
            a = reader.name().toString();
          //  if(a == "login")
          //      readLoginElement();
            reader.readNext();
        }
        else
            reader.readNext();
    }
    file.close();
    if(reader.hasError())
    {
        qDebug() << reader.errorString();
    }
    return true;
}

XmlOperator* XmlStreamReader::find(const QString& Ip)
{
    std::map<QString,XmlOperator*>::iterator it = m_map.find(Ip);
    if(it != m_map.end())
        return it->second;
    return NULL;
}

