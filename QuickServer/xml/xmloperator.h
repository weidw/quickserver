#ifndef XMLOPERATOR_H
#define XMLOPERATOR_H

#include <QString>
#include <QXmlStreamReader>

class XmlOperator
{
public:
    XmlOperator();
    virtual QString analysis(QXmlStreamReader* xmlreader);
    virtual QByteArray notice();
};

#endif // XMLOPERATOR_H
