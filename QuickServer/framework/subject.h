#ifndef SUBJECT_H
#define SUBJECT_H
#include "observer.h"
#include <QString>

class Subject
{
public:
    Subject();
    virtual void addObserver(Observer* o);
    virtual void deleteObject(Observer* o);
    virtual void notifyObject(const QByteArray data);
    virtual void setChanged() = 0;
protected:
    QString     m_name;
};

#endif // SUBJECT_H
