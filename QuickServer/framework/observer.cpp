#include "observer.h"
#include <QDebug>

Observer::Observer()
{
    m_name = "";
}

bool Observer::operator == (const Observer& o)
{
    if(this->m_name == o.m_name)
        return true;
    return false;
}

void Observer::update(const QByteArray& data)
{
    qDebug() << "i`m  ObServer";
}
