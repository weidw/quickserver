#ifndef OBSERVER_H
#define OBSERVER_H

#include <qstring>

class Observer
{
public:
    Observer();
    bool operator == (const Observer& o);
    virtual void update(const QByteArray& data);
public:
    QString         m_name;
};

#endif // OBSERVER_H
