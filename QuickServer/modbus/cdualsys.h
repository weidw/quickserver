#ifndef CDUALSYS_H
#define CDUALSYS_H
#include "canalysis.h"

class CDualSys :public CAnalysis
{
public:
    CDualSys();
    int RecvData(QByteArray str); // 接收数据
    QByteArray SendSelAllData();
protected:
    void analyState(int nData);
    void analyFault(int nData);
};

#endif // CDUALSYS_H
