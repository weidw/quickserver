#ifndef CANALYSIS_H
#define CANALYSIS_H
#include <QString>
#include <map>
#include "../afxpublic.h"
#include <queue>

// 协议解析类用于解析协议，拼接协议

class CAnalysis
{
public:
    CAnalysis();
    virtual QByteArray SendData(int nDeviId,int nFuncId,int nDS,int nNum);   // 发送查询数据
    virtual QByteArray SetData(int nDeviId,int nFuncId,int nDS,int nData);   // 发送设置数据
    virtual int RecvData(QByteArray str);   // 接收数据

    virtual QByteArray SendSelAllData();                // 返回查询所有消息指令
    bool addMember(int index,modbusTypeEnum type);      // 添加实时数据
    bool eraseMember(int index);                        // 删除指定数据
    std::queue<showbaseData> getqData();                // 获取实时数据
    modbusTypeEnum                      m_RecvSetType;  // 设置返回类型
    QByteArray                          m_selAllData;   // 查询指令
protected:
    virtual bool stateDate(int nDS);                    // 返回 设定数据 状态
    virtual bool updata(int index,int nDate);
    virtual bool updata(int index,QString strDate);
    bool pushShowData(int index,int nDate);
    bool pushShowData(int index,QString strDate);

    virtual void analyState(int nData); // 状态解析
    virtual void analyFault(int nData); // 故障解析
    bool    m_bState;       // 装填读数据，或写数据状态
    std::map<int,modbusTypeEnum>    m_map;
    std::queue<showbaseData>        m_qData;
};

#endif // CANALYSIS_H
