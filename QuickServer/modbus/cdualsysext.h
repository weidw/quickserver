#ifndef CDUALSYSEXT_H
#define CDUALSYSEXT_H
#include "canalysis.h"

class CDualSysEx :public CAnalysis
{
public:
    CDualSysEx();
    int RecvData(QByteArray str);   // 接收数据
    QByteArray SendSelAllData();
protected:
    void analyState(int nData);
    void analyFault(int nData);
};

#endif // CDUALSYSEXT_H
