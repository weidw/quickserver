#include "canalysis.h"
#include "crc16.h"
#include "myhelper.h"
#include <QDataStream>
#include "../xml/xmlopt.h"

// 基础协议
CAnalysis::CAnalysis()
{
    m_bState = false;
    m_RecvSetType = E_MODBUS_ERROR;
    m_selAllData = "";
}

QByteArray CAnalysis::SendData(int nDeviId,int nFuncId,int nDS,int nNum)
{
    QByteArray strData;
    QDataStream out(&strData,QIODevice::WriteOnly);

    out << quint8(nDeviId) << quint8(nFuncId) << quint16(nDS) << quint16(nNum);
    out << CRC16::GetCRC16((quint8*)strData.data(),strData.count());
   // CMyDebug::getInstance()->record("SendDate:" + myHelper::ByteArrayToHexStr(strData));
    return strData;
}

QByteArray CAnalysis::SetData(int nDeviId,int nFuncId,int nDS,int nData)
{
    QByteArray strData;
    QDataStream out(&strData,QIODevice::WriteOnly);

    out << quint8(nDeviId) << quint8(nFuncId) << quint16(nDS) << quint16(nData);
    out << CRC16::GetCRC16((quint8*)strData.data(),strData.count());
   // CMyDebug::getInstance()->record("SetDate:" + myHelper::ByteArrayToHexStr(strData));
    return strData;
}

int CAnalysis::RecvData(QByteArray str)
{
    /*************
     * ID 功能 起始地址H 地址L 数据长度 数据H 数据L CrcH CrcL
     * ID 功能 起始地址H 地址L 数据H 数据L  CrcH CrcL
     ***/
    QDataStream in(str);
    quint8 deviId;
    quint8 funcId;
    quint8 dataLen;
    quint16 DS,Data;
    quint16 CRC,TempCRC;

    int strLen = str.count();
    in >> deviId >> funcId >> DS >> dataLen;
    if(funcId == 5 )    // 设置返回
    {
        in.device()->seek(6); // 指针指向CRC前
        in >> CRC;
        TempCRC = CRC16::GetCRC16((quint8*)str.data(),6);
        if(CRC == TempCRC)
        {
            stateDate(DS);
            return source_recv_sellocdata;
        }
        return source_recv_error;
    }

    if(strLen < dataLen+7)        // 长度错误
        return source_recv_error;

    in.device()->seek(dataLen+5); // 指针指向CRC前
    in >> CRC;

    TempCRC = CRC16::GetCRC16((quint8*)str.data(),dataLen+5);
    if(CRC != TempCRC)          // CRC 检验错误
        return source_recv_error;
    while(m_qData.size())
        m_qData.pop();
    if(!(funcId == 3 && DS == 0))   // 无效功能码
        return source_recv_error;
    in.device()->seek(5);      // 指针指向DS后
    int i = DS;
    QString strTemp = "";
    while(dataLen+5 > in.device()->pos())
    {
        in >> Data;
        if((i >= 1 && i <= 5) ||(i >= 7 && i <= 8) ||
            (i >= 11 && i <= 12) ||i == 21)
        {
            if(Data == 202)
                strTemp = "探头短路";
            else if(Data == 201)
                strTemp = "探头开路";
            else
                Data -= 40;
        }
        updata(i++,Data);
    }
    return source_recv_reldata;
}
QByteArray CAnalysis::SendSelAllData()
{
    m_selAllData = SendData(1,3,0,22);
    return m_selAllData;
}

#include <qdebug.h>
bool CAnalysis::stateDate(int nDS)
{
    m_RecvSetType = E_MODBUS_ERROR;
    qDebug() << "nDs:" << nDS ;
    switch (nDS) {
        case 0x40 : // 开关状态
            m_RecvSetType = E_STATE_ONOFF;break;
        case 0x41:  // 模式
            m_RecvSetType = E_STATE_MODE;break;
        case 0x42:  // 制热设定
            m_RecvSetType = E_TEMP_SETHEAT;break;
        case 0x43:  // 制冷设定
            m_RecvSetType = E_TEMP_SETCOOL;break;
        case 0x44:  // 温度回差
            m_RecvSetType = E_TEMP_DIFF;break;
        case 0x45:  // 除霜进入温度
            m_RecvSetType = E_TEMP_FROSTINTO;break;
        case 0x46:  // 除霜退出温度
            m_RecvSetType = E_TEMP_FROSROUTPUT;break;
        case 0x47:  // 除霜运行时间
            m_RecvSetType = E_TIME_FROSTRUN;break;
        case 0x48:  // 除霜周期
            m_RecvSetType = E_CYCLE_FROST;break;
        case 0x49:  // 防冻运行时间
            m_RecvSetType = E_TIME_FROZENRUN;break;
        case 0x4A:  // 防冻间隔
            m_RecvSetType = E_CYCLE_FROZEN;break;
        case 0x4B:  // 排气保护温度
            m_RecvSetType = E_TEMP_PROTECTEXHAUST;break;
        case 0x4C:  // 除霜状态
            m_RecvSetType = E_STATE_FROST;break;
        case 0x4D:  // 水泵模式
            m_RecvSetType = E_STATE_PUMP;break;
    default:
        return false;
    }
    return true;
}

bool CAnalysis::addMember(int index,modbusTypeEnum type)
{
    std::map<int,modbusTypeEnum>::iterator it = m_map.find(index);
    if(it != m_map.end())
    {
        it->second = type;
        return false;
    }
    m_map[index] = type;
    return true;
}

bool CAnalysis::eraseMember(int index)
{
    std::map<int,modbusTypeEnum>::iterator it = m_map.find(index);
    if(it != m_map.end())
    {
        m_map.erase(it);
        return true;
    }
    return false;
}

std::queue<showbaseData> CAnalysis::getqData()
{
    return m_qData;
}

bool CAnalysis::updata(int index,int nDate)
{
    if(index == 0)          // 状态代码
        analyState(nDate);
    else if(index == 10)    // 故障代码
        analyFault(nDate);
    else
    {
        return pushShowData(index,nDate);
    }
    return true;
}

bool CAnalysis::updata(int index,QString strDate)
{
    return pushShowData(index,strDate);
}

bool CAnalysis::pushShowData(int index,int nDate)
{
    std::map<int,modbusTypeEnum>::iterator it= m_map.find(index);
    if(it != m_map.end())
    {
        m_qData.push(showbaseData(
                              elementTypeName::getInstance()->selElementName(it->second),
                              QString::number(nDate)));
    }
    else
        return false;
    return true;
}

bool CAnalysis::pushShowData(int index,QString strDate)
{
    std::map<int,modbusTypeEnum>::iterator it= m_map.find(index);
    if(it != m_map.end())
    {
        m_qData.push(showbaseData(
                              elementTypeName::getInstance()->selElementName(it->second),
                              strDate));
    }
    else
        return false;
    return true;
}

void CAnalysis::analyState(int nData)
{
    QString str = QString::number(nData,2);
    int nLen = str.length();
    for(int i=0;i<=10;i++)
    {
        QString strState;
        if(nLen-i <0)
            strState= "关";
        else
            strState = str.mid(nLen-i-1,1).toInt()? "开":"关";
        if(i == 8)  // 模式
        {
            if("开" == strState)
                strState = "制冷";
            else
                strState = "制热";
        }
        else if(i == 10)
        {
            if("开" == strState)
                strState = "单水泵";
            else
                strState = "双水泵";
        }
        pushShowData(i+100,strState);
    }
}

void CAnalysis::analyFault(int nData)
{
    QString str = QString::number(nData,2);
    int nLen = str.length();
    for(int i=0;i<=7;i++)
    {
        QString strState;
        if(nLen-i <0)
            strState= "正常";
        else
            strState = str.mid(nLen-i-1,1).toInt()? "报警":"正常";
        pushShowData(i+200,strState);
    }
}
