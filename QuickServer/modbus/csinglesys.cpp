#include "csinglesys.h"
#include "crc16.h"
#include "myhelper.h"
#include <QDataStream>

// 单系统所用协议
/****************************
 * 发送协议为：  nDeviId fucnID DS Lenth CRC
 *               1       1     2   2     2
 * 发送数据的 Lenth 为 数据偏移地址
 *
 * 接收协议为：  nDeviId FuncID DS  lenth Date CRC
 *                1      1     2    1     2N   2
 * 接收数据的 lenth 为 Date的2N的长度
 *
 ***************************/
CSingleSys::CSingleSys()
{
    // 开关状态
    addMember(100,E_STATE_ONOFF);
    addMember(101,E_STATE_ENDPUMP);
    addMember(102,E_STATE_COOLOUTPUT);
    addMember(103,E_STATE_CYCLEPUMP);
    addMember(104,E_STATE_FAN);
    addMember(105,E_STATE_FOURVALUE);
    addMember(106,E_STATE_PRESSUREAIR);
    addMember(107,E_STATE_HEAT);
    addMember(108,E_STATE_MODE);
    addMember(109,E_STATE_FROST);
    addMember(110,E_STATE_PUMP);

    addMember(1,E_TEMP_INTO);
    addMember(2,E_TEMP_OUTPUT);
    addMember(3,E_TEMP_AMBIENT);
    addMember(4,E_TEMP_BOX);
    addMember(5,E_TEMP_COIL);
    addMember(6,E_TEMP_EXHAUST);
    addMember(7,E_TEMP_SETHEAT);
    addMember(8,E_TEMP_SETCOOL);
    addMember(9,E_TEMP_DIFF);
    // 故障代码
    addMember(200,E_ACC_WATER);
    addMember(201,E_ACC_LOWPRE);
    addMember(202,E_ACC_HIGPRE);
    addMember(203,E_ACC_EXHAUSTHIEGH);
    addMember(204,E_ACC_TWOVALUE);
    addMember(205,E_ACC_OUTPUTHIGH);
    addMember(206,E_ACC_OUTPUTLOW);
    addMember(207,E_ACC_PHASE);

    addMember(11,E_TEMP_FROSTINTO);
    addMember(12,E_TEMP_FROSROUTPUT);
    addMember(13,E_TIME_FROSTRUN);
    addMember(14,E_CYCLE_FROST);
    addMember(15,E_TIME_FROZENRUN);
    addMember(16,E_CYCLE_FROZEN);
    addMember(17,E_TEMP_PROTECTEXHAUST);
    addMember(18,E_STEP_NUM);
    addMember(19,E_HOST_AMP);
    addMember(20,E_HOST_FLOW);
    addMember(21,E_TEMP_ROOT);
}

QByteArray CSingleSys::SendData(int nDeviId,int nFuncId,int nDS,int nNum)
{
    QByteArray strData;
    QDataStream out(&strData,QIODevice::WriteOnly);

    out << quint8(nDeviId) << quint8(nFuncId) << quint16(nDS) << quint16(nNum);
    out << CRC16::GetCRC16((quint8*)strData.data(),strData.count());
    return strData;
}

QByteArray CSingleSys::SetData(int nDeviId,int nFuncId,int nDS,int nData)
{
    QByteArray strData;
    QDataStream out(&strData,QIODevice::WriteOnly);

    out << quint8(nDeviId) << quint8(nFuncId) << quint16(nDS) << quint16(nData);
    out << CRC16::GetCRC16((quint8*)strData.data(),strData.count());
    return strData;
}

int CSingleSys::RecvData(QByteArray str)
{
    QDataStream in(str);
    quint8 deviId;
    quint8 funcId;
    quint8 dataLen;
    quint16 DS,Data;
    quint16 CRC,TempCRC;

    int strLen = str.count();
    in >> deviId >> funcId >> DS >> dataLen;
    if(funcId == 5 )    // 设置返回
    {
        in.device()->seek(6); // 指针指向CRC前
        in >> CRC;
        TempCRC = CRC16::GetCRC16((quint8*)str.data(),6);
        if(CRC == TempCRC)
        {
            stateDate(DS);
            return source_recv_sellocdata;
        }
        return source_recv_error;
    }

    if(strLen < dataLen+7)        // 长度错误
        return source_recv_error;

    in.device()->seek(dataLen+5); // 指针指向CRC前
    in >> CRC;

    TempCRC = CRC16::GetCRC16((quint8*)str.data(),dataLen+5);
    if(CRC != TempCRC)          // CRC 检验错误
        return source_recv_error;

    while(m_qData.size())
        m_qData.pop();
    if(!(funcId == 3 && DS == 0))
        return source_recv_error;

    in.device()->seek(5);      // 指针指向DS后
    int i = DS;
    QString strTemp = "";
    while(dataLen+5 > in.device()->pos())
    {
        in >> Data;
        if((i >= 1 && i <= 8) ||
            (i >= 11 && i <= 12) ||i == 21)
        {
            if(Data == 202)
                strTemp = "探头短路";
            else if(Data == 201)
                strTemp = "探头开路";
            else if(i == 6) // 排气温度
                strTemp = QString::number(Data);
            else
                strTemp = QString::number(Data - 40);
            updata(i++,strTemp);
            continue;
        }
        updata(i++,Data);
    }
    return source_recv_reldata;
}

void CSingleSys::analyState(int nData)
{
    QString str = QString::number(nData,2);
    int nLen = str.length();
    for(int i=0;i<=11;i++)
    {
        QString strState;
        if(nLen-i <0)
            strState= "关";
        else
            strState = str.mid(nLen-i-1,1).toInt()? "开":"关";
        if(i == 8)  // 模式
        {
            if("开" == strState)
                strState = "制冷";
            else
                strState = "制热";
        }
        else if(i == 10)
        {
            if("开" == strState)
                strState = "单水泵";
            else
                strState = "双水泵";
        }
        pushShowData(i+100,strState);
    }
}

void CSingleSys::analyFault(int nData)
{
    QString str = QString::number(nData,2);
    int nLen = str.length();
    for(int i=0;i<=10;i++)
    {
        QString strState;
        if(nLen-i <0)
            strState= "正常";
        else
            strState = str.mid(nLen-i-1,1).toInt()? "报警":"正常";
        pushShowData(i+200,strState);
    }
}
