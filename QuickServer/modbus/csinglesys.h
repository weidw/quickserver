#ifndef CSINGLESYS_H
#define CSINGLESYS_H
#include "canalysis.h"

// 单系统所用协议

class CSingleSys :public CAnalysis
{
public:
    CSingleSys();
    QByteArray SendData(int nDeviId,int nFuncId,int nDS,int nNum);   // 发送查询数据
    QByteArray SetData(int nDeviId,int nFuncId,int nDS,int nData);   // 发送设置数据
    int RecvData(QByteArray str); // 接收数据
protected:
    void analyState(int nData);
    void analyFault(int nData);
};

#endif // CSINGLESYS_H
