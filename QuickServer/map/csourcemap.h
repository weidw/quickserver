#ifndef CSOURCEMAP_H
#define CSOURCEMAP_H

#include "csocketmap.h"

class CSourceMap : public CSocketMap
{
public:
    static CSourceMap* getInstance();
    bool sendAllData();
private:
    CSourceMap();
    static CSourceMap* s_map;
};

#endif // CSOURCEMAP_H
