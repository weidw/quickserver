#include "csourcemap.h"
#include "../socket/csourceclient.h"

CSourceMap* CSourceMap::s_map = NULL;

CSourceMap::CSourceMap()
{

}

CSourceMap* CSourceMap::getInstance()
{
    if(s_map == NULL)
        s_map = new CSourceMap();
    return s_map;
}

bool CSourceMap::sendAllData()
{
    QMutexLocker locker(&m_mutex);
    std::map<QString,QTcpSocket*>::iterator it = m_map.begin();
    while(it != m_map.end())
    {
        ((CSourceClient*)it->second)->sendselSourceData();
        it++;
    }
    return false;
}
