#ifndef CSOCKETMAP_H
#define CSOCKETMAP_H
#include <QObject>
#include <qstring.h>
#include <map>
#include <QMutexLocker>
#include <QTcpSocket>
#include <QList>

class CSocketMap : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList ls   READ ls NOTIFY lsChanged)
public:
    CSocketMap(QObject* parent = 0);
    bool addMapSock(QString Ip,QTcpSocket* pClient);
    bool ersMapSock(QString Ip);

    QTcpSocket* find(const QString& Ip);

    QStringList ls(){return m_ls;}
    bool sendAllData(QByteArray str);
public slots:
    bool sendData(QString Ip,QByteArray str);
signals:
    void lsChanged();
protected:
    std::map<QString,QTcpSocket*>   m_map;
    QStringList                     m_ls;
    QMutex  m_mutex;
};

#endif // CSOCKETMAP_H
