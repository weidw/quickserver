#include "csocketmap.h"

CSocketMap::CSocketMap(QObject* parent) :QObject(parent)
{

}
bool CSocketMap::sendAllData(QByteArray str)
{
    QMutexLocker locker(&m_mutex);
    std::map<QString,QTcpSocket*>::iterator it = m_map.begin();
    while(it != m_map.end())
    {
        qint64 returnNum = it->second->write(str);
        it++;
    }
    return false;
}

bool CSocketMap::sendData(QString Ip,QByteArray str)
{
    QMutexLocker locker(&m_mutex);
    std::map<QString,QTcpSocket*>::iterator it = m_map.find(Ip);
    if(it != m_map.end())
    {
        qint64 returnNum = it->second->write(str);
        return true;
    }
    return false;
}

bool CSocketMap::addMapSock(QString Ip,QTcpSocket* pClient)
{
    QMutexLocker locker(&m_mutex);
    if(pClient == NULL){
        return false;
    }

    std::map<QString,QTcpSocket*>::iterator it = m_map.find(Ip);
    if(it != m_map.end())
    {
        it->second = pClient;
        return false;
    }
    m_map[Ip] = pClient;
#ifdef QT_DEBUG
    m_ls.push_back(Ip);
    emit lsChanged();
#endif
    return true;
}

bool CSocketMap::ersMapSock(QString Ip)
{
    QMutexLocker locker(&m_mutex);
    std::map<QString,QTcpSocket*>::iterator it = m_map.find(Ip);
    if(it != m_map.end())
    {
        m_map.erase(it);
#ifdef QT_DEBUG
        int a = m_ls.indexOf(Ip);
        if(a != -1)
        {
            m_ls.erase(m_ls.begin()+a);
            emit lsChanged();
        }
#endif
        return true;
    }
    return false;
}

QTcpSocket* CSocketMap::find(const QString& Ip)
{
    QMutexLocker locker(&m_mutex);
    std::map<QString,QTcpSocket*>::iterator it = m_map.find(Ip);
    if(it != m_map.end())
        return it->second;
    return NULL;
}

