#ifndef CUSERMAP_H
#define CUSERMAP_H

#include "csocketmap.h"

class CUserMap : public CSocketMap
{
public:
    static CUserMap* getInstance();
private:
    CUserMap();
    static CUserMap* s_map;
};

#endif // CUSERMAP_H
