#ifndef AFXPUBLIC_H
#define AFXPUBLIC_H

#include <map>
#include <QString>
#include "./map/csourcemap.h"
#include "./map/cusermap.h"
#include "cmainrecv.h"

#define source_recv_error      -1   // 错误
#define source_recv_reldata    1    // 源站点接收为实时数据
#define source_recv_sellocdata 2    // 源站点接收为设置返回

enum modbusTypeEnum{
    E_MODBUS_ERROR,             // 错误/初始化
    E_STATE_ONOFF,              // 设备状态
    E_STATE_ENDPUMP,            // 末端泵
    E_STATE_COOLOUTPUT,         // 制冷输出
    E_STATE_FROSTVALVE,         // 化霜电磁阀门
    E_STATE_CYCLEPUMP,          // 循环泵
    E_STATE_FAN,                // 风机
    E_STATE_FOURVALUE,          // 四通阀
    E_STATE_PRESSUREAIR,        // 压缩机
    E_STATE_HEAT,               // 曲轴加热
    E_STATE_MODE,               // 模式
    E_STATE_FROST,              // 除霜状态
    E_STATE_PUMP,               // 水泵模式
    E_STATE_PRESSUREAIR2,       // 压缩机2
    E_STATE_HEADVALVE,          // 制热旁通阀
    E_STATE_WATERVALVE,         // 喷液电磁阀

    E_TEMP_INTO,                // 回水温度
    E_TEMP_OUTPUT,              // 出水温度
    E_TEMP_AMBIENT,             // 环境温度
    E_TEMP_BOX,                 // 水箱温度
    E_TEMP_BOX2,                // 水箱温度2
    E_TEMP_COIL,                // 盘管温度
    E_TEMP_COIL2,               // 盘管温度2
    E_TEMP_EXHAUST,             // 排气温度
    E_TEMP_EXHAUST2,            // 排气温度2
    E_TEMP_SETHEAT,             // 制热设定
    E_TEMP_SETCOOL,             // 制冷设定
    E_TEMP_DIFF,                // 温度回差
    E_ACC_WATER,                // 水流故障
    E_ACC_LOWPRE,               // 低压报警
    E_ACC_LOWPRE2,              // 低压报警2
    E_ACC_HIGPRE,               // 高压报警
    E_ACC_HIGPRE2,              // 高压报警2
    E_ACC_EXHAUSTHIEGH,         // 排气温度过高报警
    E_ACC_EXHAUSTHIEGH2,        // 排气2温度过高报警
    E_ACC_TWOVALUE,             // 二通阀故障
    E_ACC_OUTPUTHIGH,           // 出水温度过高
    E_ACC_OUTPUTLOW,            // 出水温度过低
    E_ACC_PHASE,                // 缺相错相报警
    E_TEMP_FROSTINTO,           // 除霜进入温度
    E_TEMP_FROSROUTPUT,         // 除霜退出温度
    E_TIME_FROSTRUN,            // 除霜运行时间
    E_CYCLE_FROST,              // 除霜周期
    E_TIME_FROZENRUN,           // 防冻运行时间
    E_CYCLE_FROZEN,             // 防冻间隔
    E_TEMP_PROTECTEXHAUST,      // 排气保护温度

    E_STEP_NUM,                 // 电子膨胀阀步数1
    E_STEP_NUM2,                // 电子膨胀阀步数2
    E_HOST_AMP,                 // 主机电流
    E_HOST_FLOW,                // 主机流量
    E_TEMP_ROOT,                // 室内温度

    E_TEMP_OUTPUTCHX,           // 经济器出口温度
    E_TEMP_OUTPUTCHX2,          // 经济器出口温度2
    E_TEMP_INTOCHX,             // 经济器入口温度
    E_TEMP_INTOCHX2             // 经济器入口温度2

};

//elementTypeName* elementTypeName::s_element = NULL;

class elementTypeName
{
public:
    static elementTypeName* getInstance();
    QString selElementName(modbusTypeEnum type)
    {
        if(NULL == getInstance())
            return "";
        std::map<modbusTypeEnum,QString>::iterator it = s_map.find(type);
        if(s_map.end() != it)
            return it->second;
        return QString();
    }
    modbusTypeEnum selElementName(QString name)
    {
        if(NULL == getInstance())
            return E_MODBUS_ERROR;
        std::map<QString,modbusTypeEnum>::iterator it = s_anaMap.find(name);
        if(s_anaMap.end() != it)
            return it->second;
        return E_MODBUS_ERROR;
    }
private:
    elementTypeName()
    {
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_ONOFF     ,"state_onoff"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_ENDPUMP   ,"state_endpump"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_COOLOUTPUT,"state_cooloutput"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_FROSTVALVE,"state_forstvalve"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_CYCLEPUMP ,"state_cyclepump"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_FAN       ,"state_fan"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_FOURVALUE ,"state_fourvalue"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_PRESSUREAIR,"state_pressureair"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_HEAT      ,"state_heat"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_MODE      ,"state_mode"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_FROST     ,"state_frost"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_PUMP      ,"state_pump"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_PRESSUREAIR2  ,"state_pressureair2"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_HEADVALVE     ,"state_headvalve"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STATE_WATERVALVE    ,"state_watervalve"));

        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_INTO       ,"temp_into"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_OUTPUT     ,"temp_output"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_AMBIENT    ,"temp_ambient"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_BOX        ,"temp_box"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_BOX2       ,"temp_box2"));

        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_COIL       ,"temp_coil"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_COIL2      ,"temp_coil2"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_EXHAUST    ,"temp_exhaust"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_EXHAUST2   ,"temp_exhaust2"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_SETHEAT    ,"temp_setheat"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_SETCOOL    ,"temp_setcool"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_DIFF       ,"temp_diff"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_WATER       ,"acc_water"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_LOWPRE      ,"acc_lowpre"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_LOWPRE2     ,"acc_lowpre2"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_HIGPRE      ,"acc_highpre"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_HIGPRE2     ,"acc_highpre2"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_EXHAUSTHIEGH,"acc_exhausthiegh"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_EXHAUSTHIEGH2,"acc_exhausthiegh2"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_TWOVALUE    ,"acc_twovalue"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_OUTPUTHIGH  ,"acc_outputhigh"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_OUTPUTLOW   ,"acc_outputlow"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_ACC_PHASE       ,"acc_phase"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_FROSTINTO  ,"temp_frostinto"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_FROSROUTPUT,"temp_frostoutput"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TIME_FROSTRUN   ,"time_frostrun"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_CYCLE_FROST     ,"cycle_frost"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TIME_FROZENRUN  ,"time_frozenrun"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_CYCLE_FROZEN    ,"cycle_frozen"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_PROTECTEXHAUST,"temp_protectexhaust"));

        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STEP_NUM    ,"step_Num"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_STEP_NUM2   ,"step_Num2"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_HOST_AMP    ,"host_amp"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_HOST_FLOW   ,"host_flow"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_ROOT   ,"temp_root"));

        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_OUTPUTCHX  ,"temp_outputchx"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_OUTPUTCHX2 ,"temp_outputchx2"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_INTOCHX    ,"temp_intochx"));
        s_map.insert(std::map<modbusTypeEnum,QString>::value_type(E_TEMP_INTOCHX2   ,"temp_intochx2"));

        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("state_onoff"  ,E_STATE_ONOFF));   // 开关状态
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("state_mode"   ,E_STATE_MODE));    // 模式
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("state_pump"   ,E_STATE_PUMP));    // 水泵模式
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("temp_setheat" ,E_TEMP_SETHEAT));  // 制热设定
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("temp_setcool" ,E_TEMP_SETCOOL));  // 制冷设定
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("temp_diff"    ,E_TEMP_DIFF));     // 温度回差
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("temp_frostinto"   ,E_TEMP_FROSTINTO));    // 除霜进入温度
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("temp_frostoutput" ,E_TEMP_FROSROUTPUT));  // 除霜退出温度
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("time_frostrun"    ,E_TIME_FROSTRUN));     // 除霜运行时间
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("cycle_frost"      ,E_CYCLE_FROST));       // 除霜周期
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("time_frozenrun"   ,E_TIME_FROZENRUN));    // 防冻运行时间
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("cycle_frozen"     ,E_CYCLE_FROZEN));      // 防冻间隔
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("temp_protectexhaust"  ,E_TEMP_PROTECTEXHAUST));   // 排气保护温度
        s_anaMap.insert(std::map<QString,modbusTypeEnum>::value_type("state_frost"          ,E_STATE_FROST));   // 除霜状态
    }
    static elementTypeName* s_element;
    std::map<modbusTypeEnum,QString> s_map;     // 发送用
    std::map<QString,modbusTypeEnum> s_anaMap;  // 解析用
};

class showbaseData
{
public:
    showbaseData()
    {
        elementName = "";
        strData = "";
    }
    showbaseData(QString name,QString data):elementName(name),strData(data)
    {}
    QString     elementName;
    QString     strData;
};

class anaData{
public:
    anaData();
    anaData(QString type,QString name,QByteArray arr);
    anaData& operator = (const anaData& xml);
    QString     elementType;
    QString     elementName;
    QByteArray  returnData;
};

#endif // AFXPUBLIC_H

