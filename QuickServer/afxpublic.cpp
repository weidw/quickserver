#include "afxpublic.h"

elementTypeName* elementTypeName::s_element = NULL;

elementTypeName* elementTypeName::getInstance()
{
    if(NULL == s_element)
        s_element = new elementTypeName();
    return s_element;
}

anaData::anaData()
{
}

anaData::anaData(QString type,QString name,QByteArray arr) : elementType(type),elementName(name),returnData(arr)
{
}

anaData& anaData::operator = (const anaData& xml)
{
    this->elementName   = xml.elementName;
    this->returnData    = xml.returnData;
    this->elementType   = xml.elementType;
}
