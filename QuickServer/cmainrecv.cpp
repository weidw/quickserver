#include "cmainrecv.h"
#include "./map/csourcemap.h"
#include "./socket/csourceclient.h"

CMainRecv* CMainRecv::s_recv = NULL;

CMainRecv* CMainRecv::getInstance()
{
    if(s_recv == NULL)
        s_recv = new CMainRecv();
    return s_recv;
}

CMainRecv::CMainRecv(QObject *parent) : QObject(parent)
{
    connect(&m_timer,SIGNAL(timeout()),this,SLOT(onTimer()));
    m_timer.start(1000*1);
}

void CMainRecv::clickSourceData(int nIndex)
{
    CSourceClient* s = (CSourceClient*)CSourceMap::getInstance()->find(QString::number(nIndex+1));
    if(s != NULL)
        s->ls(&m_ls);
    else
       m_ls.clear();
    emit lsChanged();
}

void CMainRecv::onTimer()
{
    CSourceMap::getInstance()->sendAllData();
}
