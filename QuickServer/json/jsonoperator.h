#ifndef JSONOPERATOR_H
#define JSONOPERATOR_H
#include <QVariant>
#include <QString>

class JsonOperator
{
public:
    JsonOperator();
    virtual QString analysis(QVariantMap jsonreader);
    virtual QByteArray notice();
};

#endif // JSONOPERATOR_H
