#ifndef JSONOPT_H
#define JSONOPT_H
#include <queue>
#include "../afxpublic.h"
#include "jsonoperator.h"


class loginJson : public JsonOperator
{
public:
    loginJson();
    QString analysis(QVariantMap jsonreader);
    QByteArray notice();
    QString     m_strName;
    QString     m_strPWD;
    QString     m_strSYS;
};

class exitJson : public JsonOperator
{
public:
    exitJson();
    QString analysis(QVariantMap jsonreader);
    QByteArray notice();
};

class realJson : public JsonOperator
{
public:
    realJson();
    QString analysis(QVariantMap jsonreader);
    QByteArray notice();
    QByteArray notice(std::queue<showbaseData> qdata,QString sourceName);
    QString m_strSource;
};

class locJson : public JsonOperator
{
public:
    locJson();
    QString analysis(QVariantMap jsonreader);
    QByteArray notice();
    QString m_strName;
};

class setLocJson : public JsonOperator
{
public:
    setLocJson();
    QString analysis(QVariantMap jsonreader);
    QByteArray notice();
    QByteArray notice(QString sourceName,modbusTypeEnum moubType,QString data);
};

class noticeJson : public JsonOperator
{
public:
    noticeJson();
    QString analysis(QVariantMap jsonreader);
    QByteArray notice();
    QByteArray notice(QString data);
};

#endif // JSONOPT_H
