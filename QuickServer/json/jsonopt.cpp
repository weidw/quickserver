#include "jsonopt.h"
#include "../socket/csourceclient.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QDateTime>
#include "../db/dbopt.h"

loginJson::loginJson()
{
    m_strName = "";
    m_strPWD = "";
    m_strSYS = "";
}
QString loginJson::analysis(QVariantMap jsonreader)
{
    m_strName = jsonreader["username"].toString();
    m_strPWD = jsonreader["pwd"].toString();
    m_strSYS = jsonreader["sys"].toString();
    return m_strName;
}

QByteArray loginJson::notice()
{
    QVariantMap json;
    QVariantMap ElementMap;
    ElementMap.insert("username", m_strName);
    ElementMap.insert("limit", "100");
    ElementMap.insert("explain", "success");

    json.insert("login", ElementMap);

    QJsonDocument jsonDocument = QJsonDocument::fromVariant(QVariant(json));
    if (!jsonDocument.isNull()) {
        return jsonDocument.toJson();
    }
    return QByteArray();
}


exitJson::exitJson()
{}
QString exitJson::analysis(QVariantMap jsonreader)
{
    return jsonreader["username"].toString();
}
QByteArray exitJson::notice()
{
    QVariantMap json;
    QVariantMap ElementMap;
    ElementMap.insert("username", "admin");
    ElementMap.insert("explain", "success");

    json.insert("exit", ElementMap);

    QJsonDocument jsonDocument = QJsonDocument::fromVariant(QVariant(json));
    if (!jsonDocument.isNull()) {
        return jsonDocument.toJson();
    }
    return "";
}

realJson::realJson()
{}
QString realJson::analysis(QVariantMap jsonreader)
{
    m_strSource = jsonreader["loc"].toString();
    return m_strSource;
}
QByteArray realJson::notice()
{
    CSourceClient* s = (CSourceClient*)CSourceMap::getInstance()->find(m_strSource);
    if(NULL != s)
        return notice(s->m_psys->getqData(),m_strSource);
    QVariantMap json;
    QVariantMap ElementMap;
    ElementMap.insert("location_name", m_strSource);
    ElementMap.insert("date", QDateTime::currentDateTime().toString("yyyy-MM-dd hh::mm::ss"));
    ElementMap.insert("notify", "对应源站点未登陆");

    json.insert("real", ElementMap);

    QJsonDocument jsonDocument = QJsonDocument::fromVariant(QVariant(json));
    if (!jsonDocument.isNull()) {
        return jsonDocument.toJson();
    }
    return QByteArray();
}
QByteArray realJson::notice(std::queue<showbaseData> qdata,QString sourceName)
{
    QVariantMap json;
    QVariantMap ElementMap;
    ElementMap.insert("location_name", sourceName);
    ElementMap.insert("date", QDateTime::currentDateTime().toString("yyyy-MM-dd hh::mm::ss"));
    ElementMap.insert("notify", "");

    while(qdata.size())
    {
        ElementMap.insert(qdata.front().elementName,qdata.front().strData);
        qdata.pop();
    }

    json.insert("real", ElementMap);

    QJsonDocument jsonDocument = QJsonDocument::fromVariant(QVariant(json));
    if (!jsonDocument.isNull()) {
        return jsonDocument.toJson();
    }
    return QByteArray();
}

locJson::locJson()
{
    m_strName = "";
}
QString locJson::analysis(QVariantMap jsonreader)
{
    m_strName = jsonreader["username"].toString();
    return m_strName;
}
QByteArray locJson::notice()
{
    /***********************
     * {
     *"location": {
     *   "loc": [
     *       {"index": "1","name": "1.0"},
     *       {"index": "2","name": "2.0"},
     *       {"index": "3","name": "3.0"}
     *   ]
     *}
     *}
     **/
    QVariantMap json;
    QVariantMap ElementMap;
    std::queue<showbaseData> qdata;
    QString sql = "SELECT sourcename AS loc,name FROM usergraph "
                  "LEFT JOIN surceloctab ON sourcename=modeid WHERE username = '" +
                  m_strName + "'";
    dbOpt::getInstance()->selData(&qdata,sql);

    QVariantList locList;
    while(qdata.size())
    {
        QVariantMap child;
        child.insert("name",qdata.front().elementName);
        child.insert("index",qdata.front().strData);
        locList << child;
        qdata.pop();
    }
    ElementMap.insert("loc", locList);
    json.insert("location", ElementMap);

    QJsonDocument jsonDocument = QJsonDocument::fromVariant(QVariant(json));
    if (!jsonDocument.isNull()) {
        return jsonDocument.toJson();
    }
    return QByteArray();
}

setLocJson::setLocJson()
{}
QString setLocJson::analysis(QVariantMap jsonreader)
{
  QString strLocName;
    QVariantMap::const_iterator i = jsonreader.find("location_name");
    if(i != jsonreader.end())
        strLocName = i.value().toString();
    else
        return "";
    i = jsonreader.begin();
    while(i != jsonreader.end())
    {
        if("location_name" == i.key())
        {
            strLocName = i.value().toString();
        }
        else
        {
            CSourceClient* s = (CSourceClient*)CSourceMap::getInstance()->find(strLocName);
            if(NULL == s)
                return "";
            modbusTypeEnum moubType =  elementTypeName::getInstance()->selElementName(i.key());
            int nTemp = i.value().toInt();
            s->sendSetLoc(moubType,nTemp);  // 发送
        }
        i++;
    }
    return strLocName;
}
QByteArray setLocJson::notice()
{
    return QByteArray();
}
QByteArray setLocJson::notice(QString sourceName,modbusTypeEnum moubType,QString data)
{
    QVariantMap json;
    QVariantMap ElementMap;
    ElementMap.insert("location_name",sourceName);
    ElementMap.insert(elementTypeName::getInstance()->selElementName(moubType),data);
    json.insert("setloc", ElementMap);

    QJsonDocument jsonDocument = QJsonDocument::fromVariant(QVariant(json));
    if (!jsonDocument.isNull()) {
        return jsonDocument.toJson();
    }
    return QByteArray();
}

noticeJson::noticeJson()
{}
QString noticeJson::analysis(QVariantMap jsonreader)
{
    return "";
}
QByteArray noticeJson::notice()
{
    return QByteArray();
}
QByteArray noticeJson::notice(QString data)
{
    QVariantMap json;
    QVariantMap ElementMap;
    ElementMap.insert("message",data);
    json.insert("notice", ElementMap);

    QJsonDocument jsonDocument = QJsonDocument::fromVariant(QVariant(json));
    if (!jsonDocument.isNull()) {
        return jsonDocument.toJson();
    }
    return QByteArray();
}

