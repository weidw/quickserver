#include "jsonstreamreader.h"
#include <QJsonParseError>
#include <QVariant>
#include "jsonopt.h"

JsonStreamReader::JsonStreamReader()
{
    m_elementName = "";
    m_map.insert(std::map<QString,JsonOperator*>::value_type("login"     , new loginJson())   );  // 登录
    m_map.insert(std::map<QString,JsonOperator*>::value_type("exit"      , new exitJson())    );  // 退出
    m_map.insert(std::map<QString,JsonOperator*>::value_type("real"      , new realJson())    );  // 实时
    m_map.insert(std::map<QString,JsonOperator*>::value_type("location"  , new locJson())     );  // 站点
    m_map.insert(std::map<QString,JsonOperator*>::value_type("setloc"    , new setLocJson())  );  // 设置
}

anaData JsonStreamReader::operatorFunc(QIODevice* device)
{
    QString json = device->readAll();
    QJsonParseError error;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toUtf8(),&error);
    if (error.error == QJsonParseError::NoError) {
        if (jsonDocument.isObject()) {
            QVariantMap result = jsonDocument.toVariant().toMap();
            QVariantMap::const_iterator i = result.begin();
            while(i != result.end())
            {
                std::map<QString,JsonOperator*>::iterator it = m_map.find(i.key());
                if(it != m_map.end())
                {
                    QString strName = it->second->analysis(result[i.key()].toMap());
                    return anaData(i.key(),strName,
                                      it->second->notice());
                }
                i++;
            }
        }
    } else {
        qDebug() << error.errorString();
        return anaData();
    }
    return anaData();
}

void JsonStreamReader::setopt(QString elementName)
{
    std::map<QString,JsonOperator*>::const_iterator it = m_map.find(elementName);
    if(it != m_map.end())
    {
        m_pOperator = it->second;
        m_elementName = elementName;
    }
}

anaData JsonStreamReader::specifuFunc(QIODevice* device)
{
    QString json = device->readAll();
    QJsonParseError error;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(json.toUtf8(),&error);
    if (error.error == QJsonParseError::NoError) {
        if (jsonDocument.isObject()) {
            QVariantMap result = jsonDocument.toVariant().toMap();
            std::map<QString,JsonOperator*>::iterator it = m_map.find(m_elementName);
            if(it != m_map.end())
            {
                QString strName = it->second->analysis(result[m_elementName].toMap());
                return anaData(m_elementName,strName,
                                  it->second->notice());
            }
            else
                return anaData();
        }
    } else {
        qFatal(error.errorString().toUtf8().constData());
        return anaData();
    }
    return anaData();
}

QByteArray JsonStreamReader::notify (std::queue<showbaseData> qdata,QString sourceName)
{
    realJson json;
    return json.notice(qdata,sourceName);
}

QByteArray JsonStreamReader::notify (QString sourceName,modbusTypeEnum moubType,QString data)
{
    setLocJson setloc;
    return setloc.notice(sourceName,moubType,data);
}
