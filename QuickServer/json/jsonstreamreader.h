#ifndef JSONSTREAMREADER_H
#define JSONSTREAMREADER_H

#include <QIODevice>
#include <QString>
#include "jsonoperator.h"
#include "../afxpublic.h"
#include "../analysisstreamreader.h"
#include <queue>

class JsonStreamReader : public AnalysisStreamReader
{
public:
    JsonStreamReader();
    anaData operatorFunc(QIODevice* device);

    void setopt(QString elementName);
    anaData specifuFunc(QIODevice* device);
    QByteArray notify (std::queue<showbaseData> qdata,QString sourceName);
    QByteArray notify (QString sourceName,modbusTypeEnum moubType,QString data);
private:
    std::map<QString,JsonOperator*>   m_map;
    JsonOperator*    m_pOperator;
    QString     m_elementName;
};

#endif // JSONSTREAMREADER_H
